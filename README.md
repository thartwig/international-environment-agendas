International Environment Agendas
=================================

I was curious about the main topics that environmental ministries around the world are concerned with.
So I wrote a small NLP programm to extract text from their websites, tokenize the words, and visualize them in word clouds.
Additionally, the code translates all words to English, performs word embedding, and a tSNE dimensionality reduction to show word similarities and clusters in 2D.



Usage
-----
* `git clone` this repository.
* Execute the three jupyter notebooks in order.
* All required python libraries are pip-installable.
* You can add new websites and additional languages.


Requirements
------------
non-standard python libraries:
```
pip install pyenchant
pip install fugashi
pip install many_stop_words
pip install unidic-lite
pip install wordcloud
```

You need the takao fonts for the Japanese word cloud: https://launchpad.net/takao-fonts/+download
Maybe you need to set the path in `wordcloud(font_path=)` accordingly.

You need to download the glove 6B 100d word embedding file, for example from here: https://www.kaggle.com/datasets/danielwillgeorge/glove6b100dtxt



References
----------
* The word embedding is based on this tutorial: https://keras.io/examples/nlp/pretrained_word_embeddings/
* The data for the word embedding comes from the GloVe project: https://nlp.stanford.edu/projects/glove/
* Japanese tokenization: https://www.dampfkraft.com/nlp/how-to-tokenize-japanese.html
* tSNE inspired by: https://habr.com/en/company/vk/blog/449984/
* Optimal word placement: https://github.com/Phlya/adjustText
